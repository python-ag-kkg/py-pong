import json


class Highscore:
    def __init__(self, player_name):
        self.player_name = player_name
        self.highscore_catalog = self.read()
        self.highscore_catalog = sorted(self.highscore_catalog.items(), key=lambda item: item[1], reverse=True)  
        
        
    def get_text(self):
        text = ""
        for i, player in enumerate(self.highscore_catalog, 1):
            text+=(f"Top {i}: {player[0]} - Score: {player[1]} \n")
        return text
    
    def maintain_top(self, length):
        # If there are more than 5 players, remove the player with the least score
        if len(self.highscore_catalog) > length:
            self.highscore_catalog.pop()
        return self.highscore_catalog
    
    def get_player_name(self):
        return self.player_name
    
    def set_player_name(self,name):
        self.player_name=name
    
    def set_score(self, score):
        self.highscore_catalog.append((self.player_name, score))
        self.highscore_catalog = sorted(self.highscore_catalog, key=lambda item: item[1], reverse=True)
        self.highscore_catalog = self.maintain_top(5)
        self.write()

    def write(self):
        with open('highscore.json', 'w') as file:
            file.write(json.dumps(self.highscore_catalog))
                
    def read(self):
        with open('highscore.json') as file:
            return dict(json.load(file))
                          
            
            
            