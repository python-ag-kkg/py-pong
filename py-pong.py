import threading
import tkinter
import time
import menu
from ppthings import *
from coincounter import CoinCounter
import cs_deposit_count
import joystick
from highscore import *
import random_names

deposit_debug_start_value = 500

#--- Nur zum Debugging. Sonst wird nicht benötigt, wenn Münzenprüfer am RaspberryPi angeschloßen ist.
def SimulateCoinInsertion(coin_counter_obj, value):
    coin_counter_obj.deposit(value)
#------------------------------------------------------------------------


# Fensterschließung bearbeiten
def on_closing():
    global window_open
    global root
    window_open = False
    root.destroy()

            
def game_over (canvas, window):
    
    game_over_text = canvas.create_text(640,512,font="Courier 100", text="GAME OVER!", state='normal')
    window.update()
    time.sleep(2)
    canvas.itemconfig(game_over_text, state = 'hidden')
    window.update()

def analize_user_action (evt, m):
    global game_state
    global states
    global coin_counter
    global deposit_display
    global text_id  
    global highscore
    
    if game_state == states['menu']:
        if evt=='Return':
            if m.active_item == 0: #Spielen
                if coin_counter.withdraw(100):
                    deposit_display.redraw(coin_counter.getDeposit())
                    m.hide()
                    game_state = states['play']
                    highscore.set_player_name(random_names.generiere_namen())
                    for b in balls:
                        b.reset_cnt()
            if m.active_item == 1: #Anleitung
                m.hide()
                game_state = states['manual']
                m.canvas.itemconfig(text_id, state = 'normal')
                m.canvas.itemconfig(text_id, state = 'normal',text="Move right - joystick to the right\nMove left - joystick to the left")                
            if m.active_item ==2: #High Scores
                m.hide()
                game_state = states['high score']
                m.canvas.itemconfig(text_id, state = 'normal',text=highscore.get_text())
        elif evt=='Down':
            m.scroll_down()
        elif evt=='Up':
            m.scroll_up()
    elif game_state == states['manual'] or game_state == states['high score']:
        if evt=='Escape':
            m.show()
            m.canvas.itemconfig(text_id, state = 'hidden')
            game_state=states['menu']
    else:
        print ("unknown state")

def deposit_thread():
    global coin_counter
    while True:
        pulses_counted = cs_deposit_count.cs_pulses_count()
        inserted_money = cs_deposit_count.coin_values[pulses_counted]
        coin_counter.deposit(inserted_money)

if __name__ == '__main__':
    # Programm Beginn:
    root=tkinter.Tk()
    root.attributes('-fullscreen', True)
    canvas=tkinter.Canvas(root, width=1280, height=1024)

    #----- Manual text -----
    text_id = canvas.create_text(640, 512, font='Courier 30')
    canvas.itemconfig(text_id, state = 'hidden')
    
    #----- Score_string  -----
    score_id = canvas.create_text(672, 150, font='Courier 20', text='Score: 0')
    canvas.itemconfig(score_id,state = "hidden")
    
    #states of the program 
    states = {"menu": 1, "play": 2, "high score":3, "manual":4, "game over": 5}
    game_state = states['menu']
    highscore=Highscore(random_names.generiere_namen())
    # Münzenzähler Objekt 
    coin_counter = CoinCounter(deposit_debug_start_value)
    
    cs_deposit_count.gpio_init()
    cs_thread = threading.Thread(target=deposit_thread)
    cs_thread.start()
    
    # Eine Text-Liste der Menü-Einträge
    menu_items =["Spielen", "Anleitung", "High Scores"]
    # hier wird das Objekt menu der Klasse Menu erzeugt    
    menu = menu.Menu (menu_items, canvas)

    # Flagge, dass das Programmfenster
    # geöffnet ist auf True setzen 
    window_open = True
    
    # Fensterschließen Ereignis:
    root.protocol("WM_DELETE_WINDOW", on_closing)

    # joystick-Objekt erzeugen um den Schläger zu kontrollieren
    joystick_control = joystick.Joystick("/dev/input/js1")  

    paddle = Paddle(canvas, "blue") # Schläger Objekt erzeugen

    canvas.bind_all('<Return>', lambda evt, m = menu: analize_user_action(evt.keysym, m))
    canvas.bind_all('<KeyPress-Down>', lambda evt, m = menu: analize_user_action(evt.keysym, m))

    canvas.bind_all('<KeyPress-Up>', lambda evt, m = menu: analize_user_action(evt.keysym, m))
    canvas.bind_all('<Escape>', lambda evt, m = menu: analize_user_action(evt.keysym, m))
#--- Nur zum Debugging ohne den eigentlichen Münzenzähler.
#--- Sonst, wenn das Programm am RaspberryPi läuft und der Münzenroller angeschloßen ist, wird
#--- die Funktion an GPIO-Funktionaität gebunden. 
    #canvas.bind_all ('<a>', lambda evt, cc=coin_counter, value=1: SimulateCoinInsertion(evt, cc, value) )
    #canvas.bind_all ('<b>', lambda evt, cc=coin_counter, value=10: SimulateCoinInsertion(evt, cc, value) )
# ------
    canvas.pack()


    #Liste der Bälle erstellen
    balls = [Ball(canvas, paddle,"red"), Ball(canvas, paddle,"green"), Ball(canvas, paddle,"blue")]
    ball_num = 0 # Nummer des aktiven Balls
    balls[ball_num].start()
    deposit_display= DepositDisplayBoard(canvas, coin_counter.getDeposit())


    # Schleife ausführen nur wenn das Fenster geöffnet ist
    # und Spielleben sind noch übrig
    while window_open:
        total_touch_count = 0 # Totale Anzahl der Abpralle
        for ball in balls: # Alle Abprallzähler werden summiert
            total_touch_count+=ball.touch_cnt
        score_text = "Score for "+ highscore.get_player_name() +" : "+str(total_touch_count)
        canvas.itemconfig(score_id, text=score_text)
        if balls[ball_num].frozen == True:
            if ball_num<2:
                ball_num += 1
                balls[ball_num].start()
            else:
                if game_state == states['play']:
                    game_state = states['game over']
                if game_state == states['game over']:
                    game_over(canvas, root)
                    highscore.set_score(total_touch_count)
                    game_state = states['menu']
                balls[0].reset()
                balls[1].reset()
                balls[2].reset()
                ball_num = 0
                menu.show()
                balls[ball_num].start()
        balls[ball_num].draw()

        deposit_display.redraw(coin_counter.getDeposit())
        xpos=balls[ball_num].get_xpos()
        paddle.set_ref_xpos(xpos)

        if game_state==states['play']:
            canvas.itemconfig(score_id,state = "normal")
            if joystick_control.horiz_pos > 0:
                paddle.move_right()
            elif joystick_control.horiz_pos < 0:
                paddle.move_left()
        else:
            canvas.itemconfig(score_id,state = "hidden")
            paddle.calc_autonom_mode_dx()
      
           
        paddle.draw()
      
        menu.draw()
        deposit_display.redraw(coin_counter.getDeposit())
        time.sleep(0.03)
        root.update()
        