class CoinCounter:
    def __init__(self, initial_deposited_amount):
        self.depositedAmount = initial_deposited_amount

    def deposit(self, amount):
        if amount < 0:
            return 
        self.depositedAmount += amount
    
    def getDeposit(self):
        return self.depositedAmount
    
    def withdraw(self, amount):
        if amount < 0:
            return False
        if (self.depositedAmount >= amount):
            self.depositedAmount -= amount
            return True
        else:
            return False


