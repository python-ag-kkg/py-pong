import time
import tkinter

# Fensterschließung bearbeiten
def on_closing():
    global window_open
    global root
    window_open = False
    root.destroy()


# Die Menü-Klasse
# diese Klasse empfängt bei Erzeugung eines Objektes die folgenden Argumente:
# items - eine Text-Liste der Menü-Punkte z.B. ["Spielen", "Anleitung", "High-Scores"]
# canv - die Leihwand-Objekt auf dem das Menü geplottet wird
class Menu:
    def __init__(self, items, canv):
        self.canvas = canv # Leihwand-Objekt innerhalb unseres Menü-Objektes speichern um später immer den Zugriff auf die Leihwand zu haben
        self.menu_length = len(items)
        self.menu = [] # eine lokale Liste mit Menü-Daten. Diese Liste ist erstmal leer
        self.active_item = 0 # Nummer des gerade ausgewählten Menü Eintrags
        self.visible = True
        
        # für jeden Eintrag aus der uns übergebenen Menü-Liste müssen wir ein Rechteck und den Text im Rechteck zeihnen
        for i in range (self.menu_length):  
            canv_box_id = self.canvas.create_rectangle(0,0,250*1.3,40*1.3, fill = "green") # Einen grünen Rechteck zeichnen und seinen numerischen Identifikator in der Variable canv_box_id zwischenspeichern
            canv_text_id= self.canvas.create_text(100.5,23, text=items[i], fill = "white", font = ('Times New Roman', 28, 'bold')) # Ein Text-Objekt zeichnen und sein numerischen Identifikator in der Variable canv_text_id zwischenspeichern
            self.menu.append( [canv_box_id, canv_text_id] ) # Der lokalen Menü-Daten Liste folgenden Eintrag hinzufügen:  [Identifikator des Rechtecks, Identifikator des gezeichneten Textes]
                                                            # Achtung: Jeder Glied der self.menu[] Liste ist selbst eine Liste aus zwei Zahlen: Rechteck-Id und Text-Id, die uns den Zugriff 
                                                            # auf die beiden grafischen Objekte eines Menü-Punktes auf der Leihwand ermöglichen.
                                                            # zum Beispiel:
                                                            #   self.menu[0][0] - Numerischer Identifikator des Rechteckes, der dem ersten Menü-Punkt zusteht
                                                            #   self.menu[0][1] - Numerischer Identifikator des gezeichneten Textes, der dem ersten Menü-Punkt zusteht
                                                            #   self.menu[2][1] - Numerischer Identifikator des gezeichneten Textes, der dem dritten Menü-Punkt zusteht
                                                            #   self.menu[3][0] - Numerischer Identifikator des Rechteckes, der dem vierten Menü-Punkt zusteht
                  
        # Das letzte was wir machen sollen - die gerade erzeugten grafischen Objekte auf die richtigen Positionen zu setzen.
        # Das wird mit der Funktion move() des Leihwand-Objektes gemacht. Diese Funktion rufen wir über self.canvas.move() auf.
        for i in range(self.menu_length):    # Bitte beachten: die Variable i geht von 0 bis self.menu_length-1
            self.canvas.move(self.menu[i][0], 512, 324+i*150) # i wird mit 100 multipliziert und das entsprechende Objekt  
            self.canvas.move(self.menu[i][1], 575, 324+i*150) # bekommt entsprechende Verschiebung
      
    def hide (self):
        self.visible = False

    def show (self):
        self.visible = True    
        
    def set_active_item (self, item_num):
        if isinstance(item_num, int):
            if (item_num>=0 and item_num<self.menu_length):
                self.active_item = item_num

    def get_active_item (self):
        return self.active_item

    def draw(self):
        if self.visible:
            for i in range(self.menu_length):
                if (i == self.active_item):
                    self.canvas.itemconfig(self.menu[i][0], outline = "red", width = 5, state = 'normal')
                    self.canvas.itemconfig(self.menu[i][1], state = 'normal')
                else:
                    self.canvas.itemconfig(self.menu[i][0], outline = "green", width = 1, state = 'normal')
                    self.canvas.itemconfig(self.menu[i][1], state = 'normal')
        else:
            for i in range(self.menu_length):
                self.canvas.itemconfig(self.menu[i][0], state = 'hidden')
                self.canvas.itemconfig(self.menu[i][1], state = 'hidden')
                                

    def scroll_down (self):
        if self.active_item == self.menu_length-1:
            self.active_item = 0
        else:
            self.active_item += 1
        
    def scroll_up (self):
        if self.active_item == 0:
            self.active_item = self.menu_length-1
        else:
            self.active_item -= 1

    
            

# Anfang des Programms
if __name__ == '__main__':
    root=tkinter.Tk()
    canvas=tkinter.Canvas(root, width=1280, height=1024)
    canvas.pack()
    
    # Flagge, dass das Programmfenster
    # geöffnet ist auf True setzen 
    window_open = True
    # Fensterschließen Ereignis:
    root.protocol("WM_DELETE_WINDOW", on_closing)
    
    menu_items =["Spielen", "Anleitung", "High Scores"] # Eine Text-Liste der Menü-Einträge
    menu = Menu (menu_items, canvas) # hier wird das Objekt menu der Klasse Menu erzeugt
    
    while window_open:
        time.sleep(0.03)
        menu.draw()
        root.update()


