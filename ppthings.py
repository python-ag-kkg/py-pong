# Python Klassen für manche grafische Objekte im Spiel:
# Bälle, Schlagbrett, Depositanzeige
import random
import time 

# Die Ball-Klasse
class Ball:
    def __init__(self, canv, paddle, color):
        self.canvas = canv # Leinwand
        self.paddle = paddle # Schläger
        self.ball_id = canv.create_oval(10,10,50,50, fill = color)
        self.flat_ball_id = canv.create_oval(0,0,40,20, fill = color)
        self.reset()
    
    def reset(self):
        self.dx = 0  
        self.dy = 0
        self.touch_cnt = 0
     
        x_pos,y_pos = self.canvas.coords(self.ball_id)[0:2]
        x_flat_pos,y_flat_pos = self.canvas.coords(self.flat_ball_id)[0:2]
        #print ("RESET before moveto: CanvasId:",self.ball_id,"Ballkoordinaten [X,Y]:", x_pos, y_pos)
        #self.canvas.moveto(self.ball_id, 500, 10)
        self.canvas.move(self.ball_id, random.randint(50, 500)- x_pos, 10-y_pos)
        self.canvas.move(self.flat_ball_id, 0- x_flat_pos, 0-y_flat_pos)
        #x_pos,y_pos = self.canvas.coords(self.ball_id)[0:2]
        #print ("RESET after moveto: CanvasId:",self.ball_id,"Ballkoordinaten [X,Y]:", x_pos, y_pos)
        self.canvas.itemconfig(self.ball_id, state = "hidden") # Ball unsichtbar machen
        self.canvas.itemconfig(self.flat_ball_id, state = "hidden")
        self.frozen = True
    
    
    def start(self): # Anstoß
        self.frozen = False
        self.dx = random.randint(2,5)*random.choice([-1,1]) # horiz. Geschwindigkeit
        self.dy = random.randint(0,3) # vert. Geschwindigkeit
    
        self.canvas.moveto(self.ball_id, 500, 10)
#        self.canvas.move(self.ball_id, random.randint(0,450), self.dy)
        self.canvas.itemconfig(self.ball_id, state = "normal") # Ball sichtbar machen
    
    def hit_paddle(self, pos):
        paddle_pos = self.canvas.coords(self.paddle.id)
        if pos[2] >= paddle_pos[0] and pos[0] <= paddle_pos[2]:
            if pos[3] >= paddle_pos[1]-20:#-17.40: # -5px werden benötigt um den Ball schon beim Anflug zurückzukehren
                return True
        return False

    def reset_cnt(self):
        self.touch_cnt = 0
    
    def draw(self):
        #print ("Canvas Ball Id:",self.ball_id,"Ballkoordinaten [X,Y]:", self.canvas.coords(self.ball_id)[0:2])
        if self.frozen == False:
            position = self.canvas.coords(self.ball_id)
            # Vertikale Geschwindigkeit berechnen
            if self.hit_paddle (position)==True: # Schläger getroffen?
                self.dy = -self.dy
                self.touch_cnt+= 1
            elif position[3] >= 1000: # Boden erreicht?
                self.frozen = True
                self.dx = 0 # keine horizontale Bewegung
                self.dy = 0 # keine vertikale Bewegung
                            
                self.canvas.move(self.flat_ball_id, position[0], 1000)
                self.canvas.itemconfig(self.flat_ball_id, state = "normal")
                self.canvas.itemconfig(self.ball_id, state = "hidden")
            else: # Wenn weder der Schläger noch der Boden berührt wird, dann ist der Ball noch in der Luft
                windspeed =min(8.0, self.touch_cnt / 10.0)
                self.dy+=2 # Beschleunigung
                self.dx+=random.uniform(-windspeed,windspeed )
            # Horizontale Geschwindigkeit berechnen (Reflektion von Wänden)
            if position[0]<0 or position[2]>1270:
                self.dx = -self.dx
             # Den Ball bewegen:
            if self.hit_paddle (position)==True:
                self.canvas.moveto(self.ball_id, position[0], 910)
                self.canvas.move(self.ball_id, self.dx, self.dy)
            else:
                self.canvas.move(self.ball_id, self.dx, self.dy)
    
    # diese Funktion gibt die X-Koordinate des Mittelpunkt des Balls zurueck
    def get_xpos(self):
        ball_pos = self.canvas.coords(self.ball_id)
        return int((ball_pos[0]+ball_pos[2])/2)
  

        
# Die Schlaeger-Klasse
class Paddle:
    def __init__(self, canv, color):
        self.canvas = canv
        self.velocity = 0
        self.acceleration = 1 
                                       
        self.canvas_width = 1280
        self.color = color
        self.id = canv.create_rectangle(0,0,200,20, fill = self.color)
        self.canvas.move(self.id, 900, 950)
        self.ref_xpos=0


    # mit dieser Funktion wird eine horizontale 
    # Referenz-Position übermittelt und in der Variable self.ref_xpos gespeichert
    def set_ref_xpos(self,value):
        self.ref_xpos=value
    

    def velocity_change (self, direction, paddle_border_left, paddle_border_right):
        if paddle_border_left>0 and paddle_border_right < self.canvas_width:
            # hier wird eine Liste mit allen allen Koordinaten des Schläger-Rechtecks ermittelt
            pos = self.canvas.coords(self.id)
        
            # uns interessiert der horizontale Mittelpunkt des Schlägers.
            # dieser liegt in der Mitte zwischen der linken (pos[0]) und der rechten (pos[2]) horizontalen Koordinaten
            xpos=int((pos[0]+pos[2])/2)
            print(f"Refxpos:{self.ref_xpos}, Xpos: {xpos}")
            self.acceleration = abs(self.ref_xpos -xpos)/30.0
            if direction == 'r' and self.velocity < 20:
                self.velocity+=self.acceleration
            elif direction =='l' and self.velocity > -20:
                self.velocity-= self.acceleration
        else:
            if paddle_border_right >= self.canvas_width:
                self.velocity = -1
            elif paddle_border_left <= 0:
                self.velocity = 1
     
    def move_left(self):
        # hier wird eine Liste mit allen allen Koordinaten des Schläger-Rechtecks ermittelt
        pos = self.canvas.coords(self.id)
        self.velocity_change(direction = 'l', paddle_border_left = pos[0], paddle_border_right = pos[2])

        
    def move_right(self):
        # hier wird eine Liste mit allen allen Koordinaten des Schläger-Rechtecks ermittelt
        pos = self.canvas.coords(self.id)
        self.velocity_change(direction = 'r', paddle_border_left = pos[0], paddle_border_right = pos[2])

          
            
    def stop_movement(self):
        self.velocity = 0
        
    
    def calc_autonom_mode_dx(self):
        # hier wird eine Liste mit allen allen Koordinaten des Schläger-Rechtecks ermittelt
        pos = self.canvas.coords(self.id)
        
        # uns interessiert der horizontale Mittelpunkt des Schlägers.
        # dieser liegt in der Mitte zwischen der linken (pos[0]) und der rechten (pos[2]) horizontalen Koordinaten
        xpos=int((pos[0]+pos[2])/2)
        
        # weiterhin wird die horizontale Geschwindigkeit des Schägers (self.velocity) proportional zum
        # unterschied der Referenz-Koordinaten gerechnet:
        self.velocity = int( (self.ref_xpos -xpos)/5 )
        
    def draw(self):
        # hier wird eine Liste mit allen allen Koordinaten des Schläger-Rechtecks ermittelt
        pos = self.canvas.coords(self.id)
        if pos[0] <= 0 and self.velocity<0:  # Bewegungsstop, wenn linke Kante des Schlägers kleiner oder gleich Null ist.
            self.velocity = 0
        elif pos[2] >= self.canvas_width and self.velocity>0: # Bewegungsstop, wenn rechte Kante des Schlägers größer oder gleich Leinwand-Breite ist.
            self.velocity = 0
        else:
            self.canvas.move(self.id, self.velocity, 0)
            self.canvas.itemconfig(self.id,fill=self.color)
           


class DepositDisplayBoard:
    def __init__(self, canv, total_cent_value):
        self.canvas = canv
        self.last_cent_val = total_cent_value
        self.anim_font_size = [59, 47, 38, 30]
        self.final_anim_phase = len(self.anim_font_size)-1
        self.current_anim_phase = self.final_anim_phase
        self.last_timestamp = time.time()
            
        eur_val = total_cent_value // 100
        cent_val = total_cent_value % 100
        self.deposit_text = "Deposit: " + str(eur_val) +"."+str(cent_val)+"€"
        self.id = self.canvas.create_text(672, 40, text=self.deposit_text, fill = "red",font=("Helvetica", self.anim_font_size[self.final_anim_phase]))

    def redraw(self, total_cent_value):
        if total_cent_value != self.last_cent_val:
            eur_val = total_cent_value // 100
            cent_val = total_cent_value % 100
            if cent_val < 10:
                cent_val_str = "0"+str(cent_val)
            else:
                cent_val_str = str(cent_val)
            self.deposit_text = "Deposit: " + str(eur_val) +"."+ cent_val_str+"€"
            self.current_anim_phase=0
            self.canvas.itemconfig(self.id, text=self.deposit_text, font=("Helvetica", self.anim_font_size[self.current_anim_phase]))
            self.current_anim_phase+=1
            self.last_cent_val = total_cent_value
            self.last_timestamp = time.time()
            
        elif time.time()-self.last_timestamp>0.05 and self.current_anim_phase <= self.final_anim_phase :
            print (self.current_anim_phase, self.final_anim_phase)
            self.canvas.itemconfig(self.id, text=self.deposit_text, font=("Helvetica", self.anim_font_size[self.current_anim_phase]))
            self.current_anim_phase+=1
            self.last_timestamp = time.time()
            
            
