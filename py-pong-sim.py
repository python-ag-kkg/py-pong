import tkinter
import time
import menu
from ppthings import *
from coincounter import CoinCounter
#import joystick

#--- Nur zum Debugging. Sonst wird nicht benötigt, wenn Münzenprüfer am RaspberryPi angeschloßen ist.
def SimulateCoinInsertion(evt, coin_counter_obj, value):
    coin_counter_obj.deposit(value)
#------------------------------------------------------------------------


# Fensterschließung bearbeiten
def on_closing():
    global window_open
    global root
    window_open = False
    root.destroy()

            
def game_over (canvas, window):
    canvas.create_text(150,250,font="Courier 15", text="GAME OVER")
    window.update()
    time.sleep(2)
    

def analize_user_action (evt, m, p):
    global game_state
    global states

    if game_state == states['menu']:
        if evt.keysym=='Return':
            if m.active_item == 0: #Spielen
                m.hide()
                game_state = states['play']
            if m.active_item == 1: #Anleitung
                m.hide()
                game_state = states['manual']
                m.canvas.create_text(250, 250, font='Courier 15', text='Move right - Right arrow\nMove left - Left arrow')
            if m.active_item ==2: #High Scores
                m.hide()
                game_state = states['high score']
        elif evt.keysym=='Down':
            m.scroll_down()
        elif evt.keysym=='Up':
            m.scroll_up()
    elif game_state == states['play']:
        if evt.keysym=='Right':
            p.move_right()
        elif evt.keysym=='Left':
            p.move_left()
    elif game_state == states['high score']:
        if evt.keysym=='Escape':
            m.show()
            game_state=states['menu']
    elif game_state == states['manual']:
        if evt.keysym=='Escape':
            m.show()
            game_state=states['menu']
    else:
        print ("unknown state")



if __name__ == '__main__':
    # Programm Beginn:
    time.sleep(0.5)
    root=tkinter.Tk()
    canvas=tkinter.Canvas(root, width=500, height=500)
    
    #states of the program 
    states = {"menu": 1, "play": 2, "high score":3, "manual":4, "game over": 5}
    game_state = states['menu']
    
    # Münzenzähler Objekt 
    coin_counter = CoinCounter()
    
    # Eine Text-Liste der Menü-Einträge
    menu_items =["Spielen", "Anleitung", "High Scores"]
    # hier wird das Objekt menu der Klasse Menu erzeugt    
    menu = menu.Menu (menu_items, canvas)

    # Flagge, dass das Programmfenster
    # geöffnet ist auf True setzen 
    window_open = True
    
    # Fensterschließen Ereignis:
    root.protocol("WM_DELETE_WINDOW", on_closing)

    # joystick-Objekt erzeugen um den Schläger zu kontrollieren
    # joystick_paddle = joystick.Joystick("/dev/input/js0")  

    paddle = Paddle(canvas, "blue") # Schläger Objekt erzeugen

    canvas.bind_all('<Return>', lambda evt, m = menu, p=paddle: analize_user_action(evt, m, p))
    canvas.bind_all('<KeyPress-Down>', lambda evt, m = menu, p=paddle: analize_user_action(evt, m, p))
    canvas.bind_all('<KeyPress-Up>', lambda evt, m = menu, p=paddle: analize_user_action(evt, m, p))
    canvas.bind_all('<Escape>', lambda evt, m = menu, p=paddle: analize_user_action(evt, m, p))
    canvas.bind_all('<KeyPress-Right>', lambda evt, m = menu, p=paddle: analize_user_action(evt, m, p))
    canvas.bind_all('<KeyPress-Left>', lambda evt, m = menu, p=paddle: analize_user_action(evt, m, p))   
#--- Nur zum Debugging ohne den eigentlichen Münzenzähler.
#--- Sonst, wenn das Programm am RaspberryPi läuft und der Münzenroller angeschloßen ist, wird
#--- die Funktion an GPIO-Funktionaität gebunden. 
    canvas.bind_all ('<a>', lambda evt, cc=coin_counter, value=1: SimulateCoinInsertion(evt, cc, value) )
    canvas.bind_all ('<b>', lambda evt, cc=coin_counter, value=10: SimulateCoinInsertion(evt, cc, value) )
# ------
    canvas.pack()


    #Liste der Bälle erstellen
    balls = [Ball(canvas, paddle,"red"), Ball(canvas, paddle,"green"), Ball(canvas, paddle,"blue")]
    ball_num = 0 # Nummer des aktiven Balls
    balls[ball_num].start()
    deposit_display= DepositDisplayBoard(canvas, coin_counter.getDeposit())


    # Schleife ausführen nur wenn das Fenster geöffnet ist
    # und Spielleben sind noch übrig
    while window_open:
        total_touch_count = 0 # Totale Anzahl der Abpralle
        for ball in balls: # Alle Abprallzähler werden summiert
            total_touch_count+=ball.touch_cnt
        
        #score_text = "Score: "+ str(total_touch_count)
        #canvas.itemconfig(score_id, text=score_text)

        if balls[ball_num].frozen == True:
            if ball_num<2:
                ball_num += 1
                balls[ball_num].start()
            else:
                game_over(canvas, root)
                exit()
                
        balls[ball_num].draw()
        xpos=balls[ball_num].get_xpos()
        paddle.set_ref_xpos(xpos)
        
        if game_state!=states['play']:
            paddle.calc_autonom_mode_dx()
        paddle.draw()
        
        menu.draw()
        deposit_display.redraw(coin_counter.getDeposit())
        time.sleep(0.03)
        root.update()
        