import tkinter
import time
import struct
import threading
from pynput.keyboard import Key, Controller

class Joystick:
    def __init__(self, file_path):
        self.joystick_file=open(file_path, "rb")
        self.horiz_pos = 0
        self.vert_pos = 0
        self.btn1_pressed = False
        self.btn2_pressed = False
        self.joystick_thread = threading.Thread(target=self.read_state)
        self.joystick_thread.start()
        self.keyboard=Controller()
    def read_state(self):
        while True:
            read_joystick_str = self.joystick_file.read(8)
            #print(read_joystick_str)
            t, axis_val, code, axis_index = struct.unpack("<Ihbb", read_joystick_str)
            # print (t, axis_val, code, axis_index)
            time.sleep(0.01)
            # code: 1 for buttons; 2 for joystick
            if (code ==2 and axis_index == 1):
                if (axis_val > 0):
                    self.keyboard.press(Key.down)
                    self.keyboard.release(Key.down)
                    self.vert_pos = -1
                elif (axis_val < 0):
                    self.keyboard.press(Key.up)
                    self.keyboard.release(Key.up)
                    self.vert_pos = 1
                else:
                    self.vert_pos = 0
            if (code ==2 and axis_index == 0):
                if (axis_val > 0):
                    self.horiz_pos = 1
                elif (axis_val < 0):
                    self.horiz_pos = -1
                else:
                    self.horiz_pos = 0        
            if (code ==1 and axis_index == 2):
                if (axis_val == 1):
                    self.keyboard.press(Key.enter)
                    self.keyboard.release(Key.enter)
                    self.btn1_pressed = True
                else:
                    self.btn1_pressed = False

# Die Rechteck-Klasse
class Rectangle:
    def __init__(self, canv, color, dx, dy):
        self.canvas = canv
        self.id = canv.create_rectangle(10,10,25,25, fill = color)
        self.dx = dx
        self.dy = dy
        self.color = color
        self.canvas.move(self.id, 200, 300)
        
    def draw(self, dx, dy, color):
        # Vertikale Geschwindigkeit berechnen
        self.dy = dy
        # Horizontale Geschwindigkeit berechnen (Reflektion von Wänden)
        self.dx = dx
        # Gegebenenfalls Farbe ändern:
        if (color != self.color):
            self.canvas.itemconfig(self.id,fill = color)
            self.color = color
        # Rechteck bewegen:
        self.canvas.move(self.id, self.dx, self.dy)

        



if __name__ == '__main__':
# Programm Beginn:
    joystick=Joystick("/dev/input/js1")

    root=tkinter.Tk()
    canvas=tkinter.Canvas(root, width=500, height=500)
    canvas.pack()

    rect = Rectangle(canvas, "red", dx=5,dy=0)


    while True:
        if (joystick.btn1_pressed):
            color = "green"
        else:
            color = "red"
        rect.draw(joystick.horiz_pos*5, joystick.vert_pos*5, color)
        time.sleep(0.03)
        root.update()
