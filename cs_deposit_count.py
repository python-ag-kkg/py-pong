import RPi.GPIO as GPIO
import time
import threading
from coincounter import CoinCounter

coin_values = {0:0, 1:10, 2:20, 3:50, 4:100, 5:200}


# function gpio_init():
# to set up GPIOs 
def gpio_init():
    GPIO.setmode (GPIO.BCM)
    GPIO.setup (27, GPIO.IN)

# function wait_GPIO_rising_edge (gpio_num - BCM pin number, timeout - in seconds)
# if timeout - return False
# if rising edge detected - return True
def wait_GPIO_rising_edge(gpio_num, timeout):
    last_time_stamp = time.time()
    time_stamp = time.time()
    while time_stamp-last_time_stamp < timeout:
        time.sleep(0.01)
        if GPIO.input(gpio_num) == 1:
            return True
        time_stamp = time.time()
    return False

# function wait_GPIO_falling_edge (gpio_num - BCM pin number, timeout - in seconds)
# if timeout - return False
# if falling edge detected - return True
def wait_GPIO_falling_edge(gpio_num, timeout):
    last_time_stamp = time.time()
    time_stamp = time.time()
    while time_stamp-last_time_stamp < timeout:
        time.sleep(0.01)
        if GPIO.input(gpio_num) == 0:
            return True
        time_stamp = time.time()
    return False


def cs_pulses_count():
    counts = 0   
    while wait_GPIO_rising_edge(27,0.2):
        wait_GPIO_falling_edge(27, 0.2)
        counts += 1
    return counts    
        

