import random

# Tiere nach Geschlecht kategorisiert
maennliche_tiere = [
    "Löwe", "Tiger", "Bär", "Wolf", "Panther", "Leopard", "Elefant", "Affe", "Panda",
    "Delphin", "Wal", "Hai", "Rochen", "Pinguin", "Eisbär", "Otter", "Biber", "Rabe", "Storch",
    "Kojote", "Puma", "Jaguar", "Triceratops", "Velociraptor", "Tyrannosaurus", "Fisch", "Gecko", "Schwan", "Dodo", "HerrVaeßen",
    "HerrBaucke","HerrKraus","KaiserKarl","Hund", "Flamingo"
]

weibliche_tiere = [
    "Giraffe", "Eule", "Schlange", "Möwe", "Fledermaus", "Taube", "Eidechse", "Maus",
    "Ratte", "Robbe", "Ente", "Gans", "Ziege", "Kuh", "Hyäne", "Gazelle", "Kobra",
    "Antilope", "Mamba", "Natter", "Hyäne", "FrauHerrmann", "FrauKönig","FrauKolander","Katze"
]

saechliche_tiere = [
    "Zebra", "Krokodil","Seepferdchen", "Chamäleon", "Meerschweinchen", 
    "Känguru", "Kamel", "Schaf", "Pferd", "Rentier", "Reh", "Wiesel", "Stinktier", "Warzenschwein",
    "Gnu", "Mammut", "Megalodon", "Nashorn"
]

# Adjektive
adjektive = [
    "schnell", "stark", "mutig", "klug", "freundlich", "lustig", "wild", "tapfer", "geschickt", "majestätisch",
    "elegant", "fleißig", "weise", "kraftvoll", "sanft", "launisch", "neugierig", "schlau", "verspielt", "treu"
]

# Funktion zur Anpassung der Adjektiv-Endung
def passe_adjektiv_an(adjektiv, genus):
    if genus == "maennlich":
        if adjektiv.endswith("e"):
            return adjektiv + "r"
        else:
            return adjektiv + "er"
    elif genus == "weiblich":
        if adjektiv.endswith("e"):
            return adjektiv  # Adjektiv endet schon auf "e"
        else:
            return adjektiv + "e"  # Anfügen von "e" für weiblich
    elif genus == "saechlich":
        if adjektiv.endswith("s") or adjektiv.endswith("ß"):
            return adjektiv + "es"  # Anfügen von "es" für sächlich
        else:
            return adjektiv + "es"  # Anfügen von "es" für sächlich
    return adjektiv

def generiere_namen():
    global adjektive
    global maennliche_tiere
    global weibliche_tiere
    global saechliche_tiere
    genus = random.choice(["maennlich", "weiblich", "saechlich"])
    if genus == "maennlich":
        tier = random.choice(maennliche_tiere)
    elif genus == "weiblich":
        tier = random.choice(weibliche_tiere)
    else:
        tier = random.choice(saechliche_tiere)
    
    adjektiv = random.choice(adjektive)
    adjektiv_angepasst = passe_adjektiv_an(adjektiv, genus)
    
    return f"{adjektiv_angepasst.capitalize()}{tier}_{random.randint(10,99)}"
